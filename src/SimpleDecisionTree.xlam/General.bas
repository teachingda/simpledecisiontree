Attribute VB_Name = "General"
'Simple Decision Tree
'https://bitbucket.org/teachingda/simpledecisiontree
'Copyright (C) 2008  Thomas Seyller

'This program is free software: you can redistribute it and/or modify
'it under the terms of the GNU General Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program.  If not, see <http://www.gnu.org/licenses/>.

'***************************************************************
'*** Additional permission under GNU GPL version 3 section 7 ***
'***************************************************************
'As a special exception to the terms and conditions of GPL version 3.0,
'Thomas Seyller hereby grants you the rights to link the
'macros to functions calls that normally comes with the standard
'built-in libraries provided by your office application programs.



'This macro modified on 2/9/2009 by William VanHoomissen
'   add "Shrink" function v1.1
'This macro modified on 5/8/2009 by William VanHoomissen
'   fix "ChangeNodeType" bug v1.2
'This macro modified on 8/7/2009 by William VanHoomissen
'   add "Flip" function v1.3
'This macro modified on 7/27/2012 by William VanHoomissen
'   fix "Grow" bug v1.4
'   corrects for instability in large trees
'This macro modified on 6/23/2016 by Jongbin Jung
'   adapt to changes in Excel by implementing Ribbon interface v1.5
   

'This program is free software: you can redistribute it and/or modify
'it under the terms of the GNU General Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program.  If not, see <http://www.gnu.org/licenses/>.

'***************************************************************
'*** Additional permission under GNU GPL version 3 section 7 ***
'***************************************************************
'As a special exception to the terms and conditions of GPL version 3.0,
'Thomas Seyller hereby grants you the rights to link the
'macros to functions calls that normally comes with the standard
'built-in libraries provided by your office application programs.


Option Private Module
Public Const APPNAME As String = "Simple Decision Tree 1.5"
'Global variables
Public SDTRibbon As IRibbonUI

Public u_curve As Variant
Public u_formu As Variant
Public inv_formu As Variant
Public gamma As Variant

Public UCurveFormulaEnabled As Boolean
Public UCurveGammaEnabled As Boolean
Public InvFormulaEnabled As Boolean
    
Public Const U_RISK_NEUTRAL = "risk_neutral"
Public Const U_DELTA = "delta"
Public Const U_CUSTOM = "custom"

Sub OnRibbonLoad(ribbonUI As IRibbonUI)
    Set SDTRibbon = ribbonUI
End Sub

'Callback for Node buttons
Sub CallbackNodes(control As IRibbonControl)
    Select Case control.Tag
        Case "NewDecNode": NewNode.NewDecNode
        Case "NewUncNode": NewNode.NewUncNode
        Case "ChangeNode": ChangeNodeType.Change_Node_Type
        
        Case "Grow": Grow.Grow
        Case "Prune": Deletion.Prune
    End Select
End Sub

' Callback to enable/disable editboxes
Sub GetEnabledEditBox(control As IRibbonControl, ByRef Enabled)
    Select Case control.Tag
        Case "UCurveFormula": Enabled = UCurveFormulaEnabled
        Case "InvFormula": Enabled = InvFormulaEnabled
        Case "UCurveGamma": Enabled = UCurveGammaEnabled
    End Select
End Sub

Sub GetSelectedItemID(control As IRibbonControl, ByRef itemID As Variant)
    If IsEmpty(u_curve) Then
        u_curve = U_RISK_NEUTRAL
    End If
    itemID = u_curve
End Sub

Sub SetUCurve(control As IRibbonControl, selectedID As String, selectedIndex As Integer)
    u_curve = selectedID
    
    Values.U_Curve_Change
    
    If SDTRibbon Is Nothing Then
        MsgBox "Oops. Simple Decision Tree has crashed." & vbNewLine & _
        "Please save and restart your workbook to make changes via Simple Decision Tree"
    Else
        SDTRibbon.Invalidate
    End If
'    SDTRibbon.InvalidateControl ("Gamma")
End Sub

Sub GetUCurveFormula(control As IRibbonControl, ByRef text)
    If IsEmpty(u_formu) Then
        u_formu = "=_"
    End If
    text = u_formu
End Sub

Sub SetUFormula(control As IRibbonControl, ByRef text)
    u_formu = text
    
    Values.U_Curve_Change
    
    If SDTRibbon Is Nothing Then
        MsgBox "Oops. Simple Decision Tree has crashed." & vbNewLine & _
        "Please save and restart your workbook to make changes via Simple Decision Tree"
    Else
        SDTRibbon.Invalidate
    End If
End Sub

Sub GetInvFormula(control As IRibbonControl, ByRef text)
    If IsEmpty(inv_formu) Then
        inv_formu = "=_"
    End If
    text = inv_formu
End Sub

Sub SetInvFormula(control As IRibbonControl, ByRef text)
    inv_formu = text
    
    Values.U_Curve_Change
    
    If SDTRibbon Is Nothing Then
        MsgBox "Oops. Simple Decision Tree has crashed." & vbNewLine & _
        "Please save and restart your workbook to make changes via Simple Decision Tree"
    Else
        SDTRibbon.Invalidate
    End If
End Sub

Sub GetGamma(control As IRibbonControl, ByRef text)
    If IsEmpty(gamma) Then
        gamma = 0
    End If
    text = gamma
End Sub

Sub SetGamma(control As IRibbonControl, ByRef text)
    gamma = text
    
    Values.U_Curve_Change
    
    If SDTRibbon Is Nothing Then
        MsgBox "Oops. Simple Decision Tree has crashed." & vbNewLine & _
        "Please save and restart your workbook to make changes via Simple Decision Tree"
    Else
        SDTRibbon.Invalidate
    End If
End Sub
