Attribute VB_Name = "ChangeNodeType"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller
    
    'This macro modified on 5/8/2009 by William VanHoomissen
    '   fix "ChangeNodeType" bug
    
    
    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.
    
Public Sub Change_Node_Type()
'Transforms an existing uncertainty node into a decision node, and vice versa

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c As Range
    Set c = ActiveCell
    Dim node As Shape
    
    'Identify the that the user wishes to modify
    rw = c.Row
    col = c.Column - 1
    node_shape_nb = NodeSearch.Find_Node_By_Row_Col(rw, col)
    If node_shape_nb = -1 Then
        Msg = "You first need to pick the node you would like to modify by selecting the cell which is located immediately to its right."
        MsgBox Msg, vbExclamation, APPNAME
        Exit Sub
    End If
    
    Set node = ws.Shapes(node_shape_nb)
    node_nb = Right(node.Name, Len(node.Name) - 5)
    tp = node.Top
    lft = node.Left
    nb_branches = 0
    For i = 1 To 9
        If NodeSearch.Find_Shape("Branch", node_nb & i) = -1 Then
            Exit For
        Else
            nb_branches = nb_branches + 1
        End If
    Next i
    
    If node.AutoShapeType = msoShapeRectangle Then
        node.Delete
        With ws.Shapes.AddShape(msoShapeOval, lft, tp, 12.75, 12.75)
            .Name = "TrNd " & node_nb
            .Fill.ForeColor.RGB = RGB(0, 128, 0)
            .LockAspectRatio = False
        End With
        'Also need to create new probabilities on the branches and possibly update the branch labels
        For i = 1 To nb_branches
            Set branch = ws.Shapes(NodeSearch.Find_Shape("Branch", node_nb & i))
            rw_branch = branch.TopLeftCell.Row
            ws.Cells(rw_branch - 1, col + 2).Value = 1 / nb_branches
            ws.Cells(rw_branch - 1, col + 2).Font.Bold = False
            If ws.Cells(rw_branch - 1, col + 3).Value = "Alternative " & i Then ws.Cells(rw_branch - 1, col + 3).Value = "Outcome " & i
        Next i
    Else
        node.Delete
        ws.Cells(rw + 1, col - 1).Value = ""
        With ws.Shapes.AddShape(msoShapeRectangle, lft, tp, 12.75, 12.75)
            .Name = "TrNd " & node_nb
            .Fill.ForeColor.RGB = RGB(153, 153, 255)
            .LockAspectRatio = False
        End With
        'Also need to update the branch labels
        For i = 1 To nb_branches
            Set branch = ws.Shapes(NodeSearch.Find_Shape("Branch", node_nb & i))
            rw_branch = branch.TopLeftCell.Row
            ws.Cells(rw_branch - 1, col + 2).Font.Bold = True
            If ws.Cells(rw_branch - 1, col + 3).Value = "Outcome " & i Then ws.Cells(rw_branch - 1, col + 3).Value = "Alternative " & i
        Next i
    End If
    
    Call Values.Tree_U_Value(node_nb)

End Sub
