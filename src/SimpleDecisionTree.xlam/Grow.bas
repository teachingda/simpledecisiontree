Attribute VB_Name = "Grow"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This macro modified on 7/27/2012 by William VanHoomissen
    '   fix "Grow" bug

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Public Sub Grow()
'Adds a new branch to an existing node

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c As Range
    Set c = ActiveCell
    Dim node_to_grow As Shape
    
    'Identify the node to which the user wishes to add a branch
    node_shape_nb = Find_Node_By_Row_Col(c.Row, c.Column)
    If node_shape_nb = -1 Then
        Msg = "You first need to pick the node you would like to grow by selecting a cell which is close-by."
        MsgBox Msg, vbExclamation, APPNAME
        Exit Sub
    End If
    Set node_to_grow = ws.Shapes(node_shape_nb)
    rw = node_to_grow.TopLeftCell.Row
    col = node_to_grow.TopLeftCell.Column
    node_nb = Right(node_to_grow.Name, Len(node_to_grow.Name) - 5)
    If node_to_grow.AutoShapeType = msoShapeRectangle Then
        new_branch_type = "Alternative"
    Else
        new_branch_type = "Outcome"
    End If
    'Call Delete_Tree_Value(node_nb)
    'Call Delete_Tree_Ancestor_Values(node_nb)
    
    'Cycle through all existing branches and identify the number of the branch to add
    'Cycle through all existing leaves and identify the one that is the lowest among those that prolong the last of the existing branches
    nb_branch_to_add = 1
    row_lowest_leaf = rw
    leaf_col = 0
    For Each s In ws.Shapes
        p = InStr(s.Name, "Leaf " & node_nb)
        lenName = Len(s.Name)
        lenNode = Len(node_nb)
       
        If p = 1 And lenName >= lenNode + 6 Then
 
        'We have found a leaf that starts from the node to which the user wishes to add a branch
            If Right(s.Name, 1) = "9" Then
                Msg = "This node already has 9 branches, you cannot add a new branch to it."
                MsgBox Msg, vbExclamation, APPNAME
                Exit Sub
            Else
                
                'look for next highest branch from selected node

                '*************************************************
                '  bug fix, WVH July 27, 2012
                
                'nb_branch_to_add = nb_branch_to_add + 1
                
                subNode = Int(Val(Mid(s.Name, lenNode + 6, 1)))
                If subNode >= nb_branch_to_add Then
                    nb_branch_to_add = subNode + 1
                End If
                
                '*************************************************
                
                row_leaf = s.TopLeftCell.Row
                leaf_col = s.TopLeftCell.Column
                If row_leaf > row_lowest_leaf Then row_lowest_leaf = row_leaf
            End If
        End If
    Next s
    last_existing_branch = nb_branch_to_add - 1
    last_existing_branch = node_nb & last_existing_branch
    nb_branch_to_add = node_nb & nb_branch_to_add

    Call Insert_Rows_After_Grow(node_nb, row_lowest_leaf + 2, leaf_col)
    Call Draw_Branch(rw, col, row_lowest_leaf + 4, nb_branch_to_add, new_branch_type, 0)
    Call Insert_Rows_Recursive(node_nb)
    Call Lengthen_Branches
    With ws.Cells(row_lowest_leaf + 4, leaf_col)
        .Value = "0"
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
    Call U_Value_Leaf(row_lowest_leaf + 4, leaf_col + 1)
    Call Values.Node_Values(node_nb)
    ws.Cells(node_to_grow.TopLeftCell.Row, node_to_grow.TopLeftCell.Column + 1).Activate

End Sub

Public Sub Insert_Rows_After_Grow(current_node_nb, rw, col)
'Inserts necessary rows when a new branch is added to an existing node,
'in the area limited to that particular existing node

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim ancestor_node As Shape

    Set ancestor_node = ws.Shapes(Find_Node(current_node_nb))
    ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
    ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
    ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
    ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
    'If current_node_nb <> "" Then
    '    Set ancestor_node = ws.Shapes(Find_Node(Left(current_node_nb, Len(current_node_nb) - 1)))
    '    Call Insert_Rows_Recursive(Left(current_node_nb, Len(current_node_nb) - 1))
    'Else
    '    ws.Range(ws.Cells(rw - 1, col - 1), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
    '    ws.Range(ws.Cells(rw - 1, col - 1), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
    'End If

End Sub
