Attribute VB_Name = "Branches"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Sub Draw_Branch(rstart, cstart, rend, branch_number, Branch_Type, Optional proba)
'Draws a tree branch from the specified starting cell to the specified ending cell

    Dim ws As Worksheet
    Set ws = ActiveSheet

    ystart = ws.Cells(rstart, cstart).Top + ws.Cells(rstart, cstart).height / 2
    xstart = ws.Cells(rstart, cstart + 1).Left
    xintermed = ws.Cells(rstart, cstart + 2).Left
    xend = ws.Cells(rstart, cstart + 5).Left
    yend = ws.Cells(rend, cstart + 5).Top + ws.Cells(rend, cstart + 5).height / 2
    Thickness = ws.Cells(rend, cstart + 5).height * 3 / 4

    With ws.Shapes.AddLine(xstart, ystart, xintermed, yend)
        .Name = "FBranch " & branch_number
    End With
    With ws.Shapes.AddLine(xintermed, yend, xend, yend)
        .Name = "Branch " & branch_number
    End With
    With ws.Shapes.AddLine(xend, yend - Thickness / 2, xend, yend + Thickness / 2)
        .Name = "Leaf " & branch_number
    End With
    
    'Adds labels to the branch
    With ws.Cells(rend - 1, cstart + 3)
         .Value = Branch_Type & " " & Right(branch_number, 1)
         .Font.Bold = True
    End With
    If Branch_Type = "Outcome" Then
        With ws.Cells(rend - 1, cstart + 2)
            .Font.Bold = False
            .NumberFormat = "General"
        End With
        If IsMissing(proba) Then
            ws.Cells(rend - 1, cstart + 2).Value = 0.5
        Else
            ws.Cells(rend - 1, cstart + 2).Value = proba
        End If
    Else
        With ws.Cells(rend - 1, cstart + 2)
            .Font.Bold = True
        End With
    End If

End Sub

Sub Lengthen_Branches()
'Extends the branches of the tree that need to be extended all the way to the value measure column

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim old_branch As Shape
    
    Mx_Stage = Max_Stage()
    For Each s In ws.Shapes
        p = InStr(s.Name, "Leaf")
        If IsNumeric(p) And p > 0 Then
            leaf_stage = Len(s.Name) - 5
            If leaf_stage < Mx_Stage Then
                'We need to extend this branch of the tree all the way to the value measure column
                Set old_branch = ws.Shapes(Find_Branch(Right(s.Name, Len(s.Name) - 5)))
                old_leafx = old_branch.Left + old_branch.width
                old_leafy = old_branch.Top
                new_leafx = ws.Cells(old_branch.TopLeftCell.Row, old_branch.TopLeftCell.Column + 3 + 5 * (Mx_Stage - leaf_stage)).Left
                s.Left = new_leafx
                branch_nb = Right(s.Name, Len(s.Name) - 5)
                xb = Find_Shape("XBranch", branch_nb)
                If xb = -1 Then
                    With ws.Shapes.AddLine(old_leafx, old_leafy, new_leafx, old_leafy)
                        .Name = "XBranch " & branch_nb
                    End With
                Else
                    With ws.Shapes(xb)
                        .Left = old_leafx
                        .Top = old_leafy
                        .width = new_leafx - old_leafx
                    End With
                End If
            End If
        End If
    Next s

End Sub

Public Sub New_FBranch(xstart, xend, ystart, yend, branch_number)
'Draws a new front branch

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    ws.Shapes(Find_Shape("FBranch", branch_number)).Delete
    With ws.Shapes.AddLine(xstart, ystart, xend, yend)
        .Name = "FBranch " & branch_number
    End With

End Sub
