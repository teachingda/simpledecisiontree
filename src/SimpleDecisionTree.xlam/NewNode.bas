Attribute VB_Name = "NewNode"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Sub NewDecNode()

    Call NewNode("Alternative")

End Sub

Sub NewUncNode()

    Call NewNode("Outcome")

End Sub

Sub NewNode(Branch_Type)
'Creates a new node located in the current cell

    Dim Msg As String
    Dim ws As Worksheet
    Dim c, cc As Range
    Dim col, rw, old_col As Integer
    Dim x, y As Double
    Dim anc As Shape
    need_to_shift_to_right = True
    
    Set ws = ActiveSheet
    Set c = ActiveCell
    col = c.Column
    old_col = col
    rw = c.Row
    
    'Change active cell if trying to create a node in the first column
    If col = 1 Then
        col = 2
        old_col = 2
        Cells(rw, col).Activate
    End If
    
    'Change active cell if trying to create a node in the first four rows
    If rw <= 4 Then
        rw = 5
        Cells(rw, col).Activate
    End If

    'Error message if there already exists a tree on this worksheet and the active cell is not admissible to add a node
    If Testing.Exists_Tree = True And Testing.Is_Admissible_Cell = 0 Then
        Msg = "You can only create a node at the end of an existing branch."
        MsgBox Msg, vbExclamation, APPNAME
        Exit Sub
    End If
    
    new_node_nb = ""
    If Testing.Is_Admissible_Cell > 0 Then new_node_nb = Testing.Is_Admissible_Cell
    'If the new node is in fact a root
    If new_node_nb = "" Then
        Call Draw_Tree_Root(rw, col)
    'If the new node is not a root
    Else
        Set anc = ws.Shapes(NodeSearch.Ancestor("Leaf", new_node_nb))
        Set cc = anc.TopLeftCell
        If col > cc.Column + 5 Then
            need_to_shift_to_right = False
            col = cc.Column + 5
            Call Deletion.Delete_Extended_Branch(new_node_nb)
        End If
        Call Deletion.Delete_Leaf(new_node_nb)
    End If
    
    Set c = ws.Cells(rw, col)
    x = c.Left
    y = c.Top
    If need_to_shift_to_right Then
        For counter = 1 To 5
            ws.Columns(col).Insert (xlShiftToRight)
        Next counter
    End If
    ws.Columns(col).ColumnWidth = 1.71
    ws.Columns(col + 1).ColumnWidth = 3
    ws.Columns(col + 2).ColumnWidth = 4
    ws.Columns(col + 3).ColumnWidth = 7
    ws.Columns(col + 4).ColumnWidth = 7
    If Branch_Type = "Alternative" Then
        With ws.Shapes.AddShape(msoShapeRectangle, x, y, 12.75, 12.75)
            .Name = "TrNd " & new_node_nb
            .Fill.ForeColor.RGB = RGB(153, 153, 255)
            .LockAspectRatio = False
        End With
    Else
        With ws.Shapes.AddShape(msoShapeOval, x, y, 12.75, 12.75)
            .Name = "TrNd " & new_node_nb
            .Fill.ForeColor.RGB = RGB(0, 128, 0)
            .LockAspectRatio = False
        End With
    End If
    Call Insert_Rows(new_node_nb, rw, old_col)
    rw = rw + 2
    
    If new_node_nb = "" Then
        Call Values.Initialize_Measures(rw, col)
    Else
        If need_to_shift_to_right = False Then
            Call Values.Split_Measures(rw, old_col - 5)
        Else
            Call Values.Split_Measures(rw, col)
        End If
    End If
    Call Branches.Draw_Branch(rw, col, rw - 2, new_node_nb & "1", Branch_Type)
    Call Branches.Draw_Branch(rw, col, rw + 2, new_node_nb & "2", Branch_Type)
    Call Branches.Lengthen_Branches
    Call Values.Tree_Values(new_node_nb)

End Sub

Sub Draw_Tree_Root(rw, col)
'Draws a tree root at the specified starting cell

    Dim ws As Worksheet
    Set ws = ActiveSheet

    ystart = ws.Cells(rw, col).Top + ws.Cells(rw, col).height / 2
    xstart = ws.Cells(rw, col).Left
    lng = 7
    If lng > ws.Cells(rw, col - 1).width Then lng = ws.Cells(rw, col - 1).width

    With ws.Shapes.AddLine(xstart, ystart, xstart - lng, ystart)
        .Name = "Root "
    End With

End Sub

Public Sub Insert_Rows(current_node_nb, rw, col)
'Inserts necessary rows when a new node is added, in the area limited to that
'particular new node

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim ancestor_node As Shape

    If current_node_nb <> "" Then
        Set ancestor_node = ws.Shapes(Find_Node(Left(current_node_nb, Len(current_node_nb) - 1)))
        ws.Range(ws.Cells(rw + 2, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw + 2, col + 6)).Insert (xlShiftDown)
        ws.Range(ws.Cells(rw + 2, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw + 2, col + 6)).Insert (xlShiftDown)
        ws.Range(ws.Cells(rw - 1, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw - 1, col + 6)).Insert (xlShiftDown)
        ws.Range(ws.Cells(rw - 1, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw - 1, col + 6)).Insert (xlShiftDown)
        Call Insert_Rows_Recursive(Left(current_node_nb, Len(current_node_nb) - 1))
    Else
        ws.Range(ws.Cells(rw - 1, col - 1), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
        ws.Range(ws.Cells(rw - 1, col - 1), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
    End If

End Sub

Public Sub Insert_Rows_Recursive(node_nb)
'Inserts necessary rows when a new node is added, in the area limited to the
'ancestors of that particular new node

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim node As Shape
    Set node = ws.Shapes(Find_Node(node_nb))
    
    'First we need to determine how many rows above and below that node and its
    'branch we need to add; the only thing we know at the moment is that there is
    'a total of 4 rows to be added
    
    max_branch_row = 0
    min_branch_row = 65535
    'Find the highest and lowest branches that are rooted in this node
    For Each s In ws.Shapes
        p = InStr(s.Name, "Branch " & node_nb)
        If p = 1 And Len(s.Name) = 8 + Len(node_nb) Then
            branch_row = s.TopLeftCell.Row
            If min_branch_row > branch_row Then min_branch_row = branch_row
            If max_branch_row < branch_row Then max_branch_row = branch_row
        End If
    Next s
    old_node_row = node.TopLeftCell.Row
    new_node_row = Round(min_branch_row + (max_branch_row - min_branch_row) / 2)
    If old_node_row < new_node_row Then
        nb_rows_above = new_node_row - old_node_row
    Else
        nb_rows_above = 0
    End If
    
    If node_nb <> "" Then
        Set ancestor_node = ws.Shapes(Find_Node(Left(node_nb, Len(node_nb) - 1)))
        col = ancestor_node.TopLeftCell.Column
        For i = 1 To nb_rows_above
            ws.Range(ws.Cells(old_node_row - 1, col + 1), ws.Cells(old_node_row - 1, col + 5)).Insert (xlShiftDown)
        Next i
        For i = 1 To 4 - nb_rows_above
            ws.Range(ws.Cells(new_node_row + 2, col + 1), ws.Cells(new_node_row + 2, col + 5)).Insert (xlShiftDown)
        Next i
    Else
        col = node.TopLeftCell.Column
        For i = 1 To nb_rows_above
            ws.Range(ws.Cells(old_node_row - 1, col - 1), ws.Cells(old_node_row - 1, col)).Insert (xlShiftDown)
        Next i
    End If
    If nb_rows_above <> 0 Then
        'Need to readjust the starting point of all front arcs of the branches starting from the node
        For Each s In ws.Shapes
            p = InStr(s.Name, "FBranch " & node_nb)
            If p > 0 And Len(s.Name) = 9 + Len(node_nb) Then
                Call New_FBranch(s.Left, s.Left + s.width, node.Top + node.height / 2, ws.Shapes(Find_Shape("Branch", Right(s.Name, Len(s.Name) - 8))).Top, Right(s.Name, Len(s.Name) - 8))
            End If
        Next s
    End If
    
    If node_nb <> "" Then
        Call Insert_Rows_Recursive(Left(node_nb, Len(node_nb) - 1))
    End If
    
End Sub























Public Sub OLD_Insert_Rows(current_node_nb, rw, col, is_leaf, for_grow)
'Inserts rows in the tree following the addition of a new node or of a new branch
'rw is the row of the former leaf where the node was originally added
'col is the column of the former leaf where the node was originally added

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim ancestor_node As Shape
    
    If current_node_nb <> "" And is_leaf Then
        Set ancestor_node = ws.Shapes(Find_Node(Left(current_node_nb, Len(current_node_nb) - 1)))
        If for_grow = False Then
            ws.Range(ws.Cells(rw + 2, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw + 2, col + 6)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw + 2, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw + 2, col + 6)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw - 1, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw - 1, col + 6)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw - 1, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw - 1, col + 6)).Insert (xlShiftDown)
        Else
            ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw, col + 6)).Insert (xlShiftDown)
        End If
    Else
        If current_node_nb <> "" Then
            ws.Range(ws.Cells(rw + 2, col - 4), ws.Cells(rw + 2, col)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw + 2, col - 4), ws.Cells(rw + 2, col)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw - 1, col - 4), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw - 1, col - 4), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
        Else
            ws.Range(ws.Cells(rw - 1, col - 1), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
            ws.Range(ws.Cells(rw - 1, col - 1), ws.Cells(rw - 1, col)).Insert (xlShiftDown)
        End If
    End If
    If current_node_nb <> "" Then
        new_node_nb = Left(current_node_nb, Len(current_node_nb) - 1)
        new_rw = ws.Shapes(Find_Node(new_node_nb)).TopLeftCell.Row
        new_col = ws.Shapes(Find_Node(new_node_nb)).TopLeftCell.Column
        Call Insert_Rows(new_node_nb, new_rw, new_col, False, for_grow)
    End If

End Sub

Public Function Nb_Rows_To_Insert_Above(node_nb) As Integer
'Returns the number of rows that need to be inserted above a specific branch given its number and given that four rows need to be inserted somehow

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim node As Shape
    Dim branch As Shape

    Set node = ws.Shapes(Find_Node(node_nb))
    min_max_branch_rows_exist = False
    max_branch_row = 0
    min_branch_row = 65535
    'Find the highest and lowest branches that are rooted in this node
    For Each s In ws.Shapes
        p = InStr(s.Name, "Branch " & node_nb)
        If p = 1 And Len(s.Name) = 8 + Len(node_nb) Then
            branch_row = s.TopLeftCell.Row
            If min_max_branch_rows_exist = False Then
                min_max_branch_rows_exist = True
                min_branch_row = branch_row
                max_branch_row = branch_row
            Else
                If min_branch_row > branch_row Then
                    min_branch_row = branch_row
                Else
                    If max_branch_row < branch_row Then
                        max_branch_row = branch_row
                    End If
                End If
            End If
        End If
    Next s
    If min_max_branch_rows_exist = False Then
        new_node_row = node.TopLeftCell.Row
    Else
        new_node_row = Round(min_branch_row + (max_branch_row - min_branch_row) / 2)
    End If
    old_node_row = node.TopLeftCell.Row
    'If new_node_row <> old_node_row Then
        'Call Delete_Tree_Value(node_nb)
        'Need to readjust the position of the node...
        'node.Top = ws.Cells(new_node_row, 1).Top
        '... and of the branch leading to it
        right_col = node.TopLeftCell.Column
        If node_nb = "" Then
            left_col = right_col - 1
            'Set branch = ws.Shapes(Find_Shape("Root", node_nb))
            'branch.Top = ws.Cells(new_node_row, 1).Top + ws.Cells(new_node_row, 1).height / 2
        Else
            left_col = right_col - 4
            'auxcol = node.TopLeftCell.Column
            'If ws.Cells(old_node_row - 1, auxcol - 2).Formula = "" Then
            '    row_adjust = 2
            'Else
            '    row_adjust = 0
            'End If
            'old_node_row = old_node_row - row_adjust
            'ws.Cells(new_node_row - 1, auxcol - 3).Formula = ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).Formula
            'ws.Cells(new_node_row - 1, auxcol - 3).Font.Bold = ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).Font.Bold
            'ws.Cells(new_node_row - 1, auxcol - 3).NumberFormat = ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).NumberFormat
            'ws.Cells(new_node_row - 1, auxcol - 2).Formula = ws.Cells(old_node_row - row_adjust - 1, auxcol - 2).Formula
            'ws.Cells(new_node_row - 1, auxcol - 2).Font.Bold = ws.Cells(old_node_row - row_adjust - 1, auxcol - 2).Font.Bold
            'ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).Formula = ""
            'ws.Cells(old_node_row - row_adjust - 1, auxcol - 2).Formula = ""
            'Set branch = ws.Shapes(Find_Shape("Branch", node_nb))
            'branch.Top = ws.Cells(new_node_row, 1).Top + ws.Cells(new_node_row, 1).height / 2
            'Set branch = ws.Shapes(Find_Shape("FBranch", node_nb))
            'Call New_FBranch(branch.Left, branch.Left + branch.width, ws.Shapes(Ancestor("TrNd", node_nb)).Top + ws.Shapes(Ancestor("TrNd", node_nb)).height / 2, ws.Cells(new_node_row, 1).Top + ws.Cells(new_node_row, 1).height / 2, node_nb)
        End If
        If new_node_row > old_node_row Then
            For i = 1 To new_node_row - old_node_row
                ws.Range(ws.Cells(old_node_row - 1, left_col), ws.Cells(old_node_row - 1, right_col)).Insert (xlShiftDown)
                ws.Range(ws.Cells(new_node_row + 1, left_col), ws.Cells(new_node_row + 1, right_col)).Delete (xlShiftUp)
            Next i
        Else
            For i = 1 To new_node_row - old_node_row
                ws.Range(ws.Cells(old_node_row - 1, left_col), ws.Cells(old_node_row - 1, right_col)).Insert (xlShiftDown)
                ws.Range(ws.Cells(new_node_row + 1, left_col), ws.Cells(new_node_row + 1, right_col)).Delete (xlShiftUp)
            Next i
        End If
        'Need to readjust the starting point of all front arcs of the branches starting from the node
        For Each s In ws.Shapes
            p = InStr(s.Name, "FBranch " & node_nb)
            If p > 0 And Len(s.Name) = 9 + Len(node_nb) Then
                Call New_FBranch(s.Left, s.Left + s.width, node.Top + node.height / 2, ws.Shapes(Find_Shape("Branch", Right(s.Name, Len(s.Name) - 8))).Top, Right(s.Name, Len(s.Name) - 8))
            End If
        Next s
    'End If

End Function
