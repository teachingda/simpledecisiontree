Attribute VB_Name = "Values"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Public Sub Delete_Tree_Value(node_nb)
'Deletes the u-value and value measure of a specified node, given its node number

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c As Range
    
    Set c = ws.Shapes(Find_Node(node_nb)).TopLeftCell
    ws.Cells(c.Row + 1, c.Column - 1).Value = ""
    If node_nb <> "" Then
        ws.Cells(c.Row + 1, c.Column - 2).Value = ""
    Else
        ws.Cells(c.Row + 2, c.Column - 1).Value = ""
    End If

End Sub

Public Sub Delete_Tree_Ancestor_Values(node_nb)
'Deletes the u-values and value measures of the ancestors of a specified node, given its node number

    If Len(node_nb) = 0 Then Exit Sub
    Call Delete_Tree_Value(Left(node_nb, Len(node_nb) - 1))
    Call Delete_Tree_Ancestor_Values(Left(node_nb, Len(node_nb) - 1))
    
End Sub

Public Sub Tree_U_Value(node_nb)
'Calculates the u-value and the CE at a specified node, given its node number

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c, cc As Range
    Dim ss As Shape
    Dim formul As String
    
    node_shape_nb = Find_Node(node_nb)
    Set c = ws.Shapes(node_shape_nb).TopLeftCell
    rw = c.Row + 1
    col = c.Column - 1
    If ws.Shapes(node_shape_nb).AutoShapeType = msoShapeOval Then
        'If the node is an uncertainty node
        formul = "="
        need_plus = False
        'Look at all shapes and check whether they are descendant leaves or descendant nodes of the current node
        For Each s In ws.Shapes
            If Is_Child_Node(s, node_nb) Then
                If need_plus Then formul = formul & "+"
                Set cc = s.TopLeftCell
                rwc = cc.Row + 1
                colc = cc.Column - 1
                Set cc = ws.Cells(rwc, colc)
                rwcp = cc.Row - 2
                colcp = cc.Column - 2
                Set ccc = ws.Cells(rwcp, colcp)
                formul = formul & ccc.Address() & "*" & cc.Address()
                need_plus = True
            Else
                If Is_Child_Leaf(s, node_nb) Then
                    If need_plus Then formul = formul & "+"
                    Set cc = s.TopLeftCell
                    rwc = cc.Row
                    colc = cc.Column + 1
                    Set cc = ws.Cells(rwc, colc)
                    Set ss = ws.Shapes(Find_Shape("Branch", Right(s.Name, Len(s.Name) - 5)))
                    rwcp = ss.TopLeftCell.Row - 1
                    colcp = ss.TopLeftCell.Column
                    Set ccc = ws.Cells(rwcp, colcp)
                    formul = formul & ccc.Address() & "*" & cc.Address()
                    need_plus = True
                End If
            End If
        Next s
        ws.Cells(rw, col).Formula = formul
    Else
        'If the node is a decision node
        formul = "=MAX("
        need_coma = False
        'Look at all shapes and check whether they are descendant leaves or descendant nodes of the current node
        For Each s In ws.Shapes
            If Is_Child_Node(s, node_nb) Then
                If need_coma Then formul = formul & ","
                Set cc = s.TopLeftCell
                rwc = cc.Row + 1
                colc = cc.Column - 1
                Set cc = ws.Cells(rwc, colc)
                formul = formul & cc.Address()
                need_coma = True
                ws.Cells(rwc - 2, colc - 2).Formula = "=IF(" & ws.Cells(rw, col).Address() & "=" & cc.Address() & "," & Chr(34) & Chr(62) & Chr(62) & Chr(62) & Chr(34) & "," & Chr(34) & Chr(34) & ")"
            Else
                If Is_Child_Leaf(s, node_nb) Then
                    If need_coma Then formul = formul & ","
                    Set cc = s.TopLeftCell
                    rwc = cc.Row
                    colc = cc.Column + 1
                    Set cc = ws.Cells(rwc, colc)
                    formul = formul & cc.Address()
                    need_coma = True
                    Set ss = ws.Shapes(Find_Shape("Branch", Right(s.Name, Len(s.Name) - 5)))
                    rwtopbranch = ss.TopLeftCell.Row - 1
                    coltopbranch = ss.TopLeftCell.Column
                    ws.Cells(rwtopbranch, coltopbranch).Formula = "=IF(" & ws.Cells(rw, col).Address() & "=" & cc.Address() & "," & Chr(34) & Chr(62) & Chr(62) & Chr(62) & Chr(34) & "," & Chr(34) & Chr(34) & ")"
                End If
            End If
        Next s
        ws.Cells(rw, col).Formula = formul & ")"
    End If

End Sub

Public Sub Tree_Value_Measure(node_nb)
'Calculates the CE at a specified node, given its node number

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c, cc As Range
    Dim ss As Shape
    
    node_shape_nb = Find_Node(node_nb)
    Set c = ws.Shapes(node_shape_nb).TopLeftCell
    rw = c.Row + 1
    col = c.Column - 2
    t = u_curve
    If t = "" Then Exit Sub
    If t = U_RISK_NEUTRAL Then
        inv_formu = "=_"
        UCurveFormulaEnabled = False
        InvFormulaEnabled = False
        UCurveGammaEnabled = False
    ElseIf t = U_DELTA Then
        UCurveFormulaEnabled = False
        InvFormulaEnabled = False
        UCurveGammaEnabled = True
    
        If Not IsNumeric(gamma) Then
            inv_formu = "=IF(gamma=0,_,if(gamma>0,-1/(gamma)*LN(1-_),-1/(gamma)*LN(_-1)))"
            If gamma <> "See Spreadsheet" Then inv_formu = Replace(u_formu, "gamma", gamma)
        Else
            If gamma = 0 Then
                inv_formu = "=_"
            Else
                If gamma > 0 Then
                    inv_formu = "=-1/" & gamma & "*LN(1-_)"
                Else
                    inv_formu = "=-1/" & gamma & "*LN(_-1)"
                End If
            End If
        End If
    ElseIf t = U_CUSTOM Then
        UCurveFormulaEnabled = True
        InvFormulaEnabled = True
        UCurveGammaEnabled = False
    End If
    u_value_cell = ws.Cells(rw, col + 1).Address()
    If node_nb = "" Then
        rw = rw + 1
        col = col + 1
    End If
    With ws.Cells(rw, col)
        .Formula = Replace(inv_formu, "_", u_value_cell)
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
End Sub

Public Sub Node_Values(node_nb)
'Calculates the u-value and value measure at a specified node, given its node number

    Call Tree_U_Value(node_nb)
    Call Tree_Value_Measure(node_nb)
    
End Sub

Public Sub Tree_Values(node_nb)
'Calculates the u-value and value measure at a specified node, given its node number, and at all ancestor nodes

    Call Tree_U_Value(node_nb)
    Call Tree_Value_Measure(node_nb)
    If Len(node_nb) = 0 Then Exit Sub
    Call Tree_Values(Left(node_nb, Len(node_nb) - 1))
    
End Sub

Public Sub Split_Measures(rw, col)
'Called when the user creates a node that is not the root of the tree
'Updates the value measure column

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    With ws.Cells(rw - 2, col + 5)
        .Value = ws.Cells(rw, col + 5).Value
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
    With ws.Cells(rw + 2, col + 5)
        .Value = ws.Cells(rw, col + 5).Value
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
    Call U_Value_Leaf(rw - 2, col + 6)
    Call U_Value_Leaf(rw + 2, col + 6)
    ws.Cells(rw, col + 5).Value = ""
    ws.Cells(rw, col + 6).Value = ""

End Sub

Public Sub U_Value_Leaf(rw, col)
'Calculates a u-value positioned at one of the leaves

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    t = u_curve
    If t = "" Then Exit Sub
    If t = U_RISK_NEUTRAL Then
        u_formu = "=_"
        UCurveFormulaEnabled = False
        UCurveGammaEnabled = False
    ElseIf t = U_DELTA Then
        UCurveFormulaEnabled = False
        UCurveGammaEnabled = True
        
        If Not IsNumeric(gamma) Then
            u_formu = "=IF(gamma=0,_,if(gamma>0,1-EXP(-gamma*_),1+EXP(-gamma*_)))"
            If gamma <> "See Spreadsheet" Then u_formu = Replace(u_formu, "gamma", gamma)
        Else
            If gamma = 0 Then
                u_formu = "=_"
            Else
                If gamma > 0 Then
                    u_formu = "=1-EXP(-" & gamma & "*_)"
                Else
                    u_formu = "=1+EXP(-" & gamma & "*_)"
                End If
            End If
        End If
    ElseIf t = U_CUSTOM Then
        UCurveFormulaEnabled = True
        UCurveGammaEnabled = False
    End If
        
    value_cell = ws.Cells(rw, col - 1).Address()
    With ws.Cells(rw, col)
        .Formula = Replace(u_formu, "_", value_cell)
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
End Sub

Public Sub Recalc_Leaf_U_Values()
'Recalculates all leaf u-values

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c As Range

    For Each s In ws.Shapes
        p = InStr(s.Name, "Leaf")
        If IsNumeric(p) And p > 0 Then
            Set c = s.TopLeftCell
            Call U_Value_Leaf(c.Row, c.Column + 1)
        End If
    Next s
    
End Sub

Public Sub Initialize_Measures(rw, col)
'Called when the user creates the root of the tree
'Creates the value measure column

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    ws.Columns(col + 5).ColumnWidth = 14
    With ws.Cells(rw - 6, col + 5)
        .Value = "Value Measure"
        .Font.Underline = True
        .Font.Bold = True
    End With
    ws.Columns(col + 6).ColumnWidth = 10
    With ws.Cells(rw - 6, col + 6)
        .Value = "U-Value"
        .Font.Underline = True
        .Font.Bold = True
    End With
    With ws.Cells(rw - 2, col + 5)
        .Value = "0"
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
    With ws.Cells(rw + 2, col + 5)
        .Value = "0"
        .NumberFormat = "General"
        .Font.Underline = False
        .Font.Bold = False
    End With
    Call U_Value_Leaf(rw - 2, col + 6)
    Call U_Value_Leaf(rw + 2, col + 6)

End Sub

Public Sub Recalc_Tree_Values()
'Recalculates all tree CE's

    Dim ws As Worksheet
    Set ws = ActiveSheet

    For Each s In ws.Shapes
        p = InStr(s.Name, "TrNd")
        If IsNumeric(p) And p > 0 Then
            Call Tree_Value_Measure(Right(s.Name, Len(s.Name) - 5))
        End If
    Next s
End Sub

Sub U_Curve_Change()
'Is called when the user changes the form of the u-curve using the combobox
    Call Recalc_Leaf_U_Values
    Call Recalc_Tree_Values
End Sub
