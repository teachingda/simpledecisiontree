Attribute VB_Name = "Deletion"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.
    
    'Global variables
    Public other_branch_found As Boolean
    Public branch_CE, branch_CE2 As Double
    Public node_nb_for_node_deletion As Integer

Sub Delete_Leaf(leaf_nb)
'Deletes a leaf given its number

    Call Delete_Shape("Leaf", leaf_nb)

End Sub

Sub Delete_Extended_Branch(branch_nb)
'Deletes a branch extension given its number

    Call Delete_Shape("XBranch", branch_nb)

End Sub

Sub Delete_Shape(shape_type, shape_nb)
'Deletes a shape (node or leaf or branch) given its shape type and number

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    f = Find_Shape(shape_type, shape_nb)
    If f > -1 Then ws.Shapes(f).Delete

End Sub

Public Sub Prune()
'Removes an existing branch or an entire node

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c As Range
    Set c = ActiveCell
    
    'Identify the leaf or node that the user wishes to delete
    rw = c.Row
    col = c.Column
    node_shape_nb = Find_Node_By_Row_Col(rw, col)
    If node_shape_nb = -1 Then
        node_shape_nb = Find_Leaf_By_Row_Col(rw, col)
        If node_shape_nb = -1 Then
            Msg = "You first need to pick the node or branch you would like to prune by selecting the cell which is located immediately to its right."
            MsgBox Msg, vbExclamation, APPNAME
            Exit Sub
        Else
            Call Prune_Leaf(node_shape_nb)
        End If
    Else
        Set node = ws.Shapes(node_shape_nb)
        col = node.TopLeftCell.Column
        If col >= 3 Then branch_CE = ws.Cells(rw + 1, col - 2).Value
        If Len(node.Name) > 5 Then node_nb_for_node_deletion = Right(node.Name, Len(node.Name) - 5)
        Call Prune_Nodes(node_shape_nb)
    End If

End Sub

Public Sub Prune_Leaf(leaf_shape_nb)
'Prunes / deletes a leaf and a branch given their number

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim leaf, shape_to_delete As Shape
    Set leaf = ws.Shapes(leaf_shape_nb)
    rw = leaf.TopLeftCell.Row
    col = leaf.TopLeftCell.Column
    
    'Delete the various shapes associated with the removal of this leaf
    leaf_number = Right(leaf.Name, Len(leaf.Name) - 5)
    Set shape_to_delete = ws.Shapes(Find_Shape("FBranch", leaf_number))
    shape_to_delete.Delete
    Set shape_to_delete = ws.Shapes(Find_Shape("Branch", leaf_number))
    shape_to_delete.Delete
    xbranch_shape_nb = Find_Shape("XBranch", leaf_number)
    If xbranch_shape_nb <> -1 Then
        Set shape_to_delete = ws.Shapes(Find_Shape("XBranch", leaf_number))
        shape_to_delete.Delete
    End If
    branch_CE2 = leaf.TopLeftCell.Value
    leaf.Delete
    'Call Delete_Tree_Ancestor_Values(leaf_number)
    
    'Need to readjust the branch and leaf numbers on the other branches and leaves
    last_digit_deleted_leaf = Right(leaf_number, 1)
    first_digits_deleted_leaf = Left(leaf_number, Len(leaf_number) - 1)
    other_branch_found = False
    Call Renumber_Branch_Siblings(first_digits_deleted_leaf, last_digit_deleted_leaf + 1)
    
    If other_branch_found Then
        Call Delete_Rows_After_Leaf_Deletion(leaf_number, rw, col, True)
        Call Readjust_Extended_Branches_After_Leaf_Deletion
        Call Node_Values(first_digits_deleted_leaf)
    Else
        'If that was the last leaf that stemmed from its parent node... Need to prune that parent node
        Call Prune_Node(Find_Node(first_digits_deleted_leaf))
    End If

End Sub

Public Sub Renumber_Branch_Siblings(first_digits, last_digit)
'Renumbers the siblings of branch number first_digits, last_digit-1

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    branch_found = False
    new_last_digit = last_digit - 1
    For Each s In ws.Shapes
        If InStr(s.Name, "Branch " & first_digits & "1") = 1 Or InStr(s.Name, "Branch " & first_digits & "2") = 1 Or InStr(s.Name, "Branch " & first_digits & "3") = 1 Or InStr(s.Name, "Branch " & first_digits & "4") = 1 Or InStr(s.Name, "Branch " & first_digits & "5") = 1 Or InStr(s.Name, "Branch " & first_digits & "6") = 1 Or InStr(s.Name, "Branch " & first_digits & "7") = 1 Or InStr(s.Name, "Branch " & first_digits & "8") = 1 Or InStr(s.Name, "Branch " & first_digits & "9") = 1 Then
            other_branch_found = True
        End If
        If InStr(s.Name, "Branch " & first_digits & last_digit) = 1 Then
            branch_found = True
            s.Name = Replace(s.Name, "Branch " & first_digits & last_digit, "Branch " & first_digits & new_last_digit)
            If Len(s.Name) = Len(first_digits) + 8 Then
                If ws.Cells(s.TopLeftCell.Row - 1, s.TopLeftCell.Column + 1).Value = "Outcome " & last_digit Then
                    ws.Cells(s.TopLeftCell.Row - 1, s.TopLeftCell.Column + 1).Value = "Outcome " & new_last_digit
                Else
                    If ws.Cells(s.TopLeftCell.Row - 1, s.TopLeftCell.Column + 1).Value = "Alternative " & last_digit Then
                        ws.Cells(s.TopLeftCell.Row - 1, s.TopLeftCell.Column + 1).Value = "Alternative " & new_last_digit
                    End If
                End If
            End If
        Else
            If InStr(s.Name, "FBranch " & first_digits & last_digit) = 1 Then
                s.Name = Replace(s.Name, "FBranch " & first_digits & last_digit, "FBranch " & first_digits & new_last_digit)
            Else
                If InStr(s.Name, "XBranch " & first_digits & last_digit) = 1 Then
                    s.Name = Replace(s.Name, "XBranch " & first_digits & last_digit, "XBranch " & first_digits & new_last_digit)
                Else
                    If InStr(s.Name, "Leaf " & first_digits & last_digit) = 1 Then
                        s.Name = Replace(s.Name, "Leaf " & first_digits & last_digit, "Leaf " & first_digits & new_last_digit)
                    Else
                        If InStr(s.Name, "TrNd " & first_digits & last_digit) = 1 Then
                            s.Name = Replace(s.Name, "TrNd " & first_digits & last_digit, "TrNd " & first_digits & new_last_digit)
                        End If
                    End If
                End If
            End If
        End If
    Next s
    If branch_found And last_digit < 9 Then Call Renumber_Branch_Siblings(first_digits, last_digit + 1)

End Sub

Public Sub Delete_Rows_After_Leaf_Deletion(current_node_nb, rw, col, is_leaf)
'Deletes rows upward in the tree following the deletion of a leaf

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim ancestor_node, node As Shape
    
    If is_leaf Then
        For i = 1 To 4
            Set ancestor_node = ws.Shapes(Find_Node(Left(current_node_nb, Len(current_node_nb) - 1)))
            ws.Range(ws.Cells(rw - 1, ancestor_node.TopLeftCell.Column + 1), ws.Cells(rw - 1, col + 1)).Delete (xlShiftUp)
        Next i
    Else
        Set node = ws.Shapes(Find_Node(current_node_nb))
        
        'First we need to determine how many rows above and below that node and its
        'branch we need to delete; the only thing we know at the moment is that there is
        'a total of 4 rows to be deleted
    
        max_branch_row = 0
        min_branch_row = 65535
        'Find the highest and lowest branches that are rooted in this node
        For Each s In ws.Shapes
            p = InStr(s.Name, "Branch " & current_node_nb)
            If p = 1 And Len(s.Name) = 8 + Len(current_node_nb) Then
                branch_row = s.TopLeftCell.Row
                If min_branch_row > branch_row Then min_branch_row = branch_row
                If max_branch_row < branch_row Then max_branch_row = branch_row
            End If
        Next s
        old_node_row = node.TopLeftCell.Row
        new_node_row = Round(min_branch_row + (max_branch_row - min_branch_row) / 2)
        nb_rows_above = old_node_row - new_node_row
    
        If current_node_nb <> "" Then
            Set ancestor_node = ws.Shapes(Find_Node(Left(current_node_nb, Len(current_node_nb) - 1)))
            lcol = ancestor_node.TopLeftCell.Column
            For i = 1 To nb_rows_above
                ws.Range(ws.Cells(new_node_row - 1, lcol + 1), ws.Cells(new_node_row - 1, lcol + 5)).Delete (xlShiftUp)
            Next i
            For i = 1 To 4 - nb_rows_above
                ws.Range(ws.Cells(new_node_row + 2, lcol + 1), ws.Cells(new_node_row + 2, lcol + 5)).Delete (xlShiftUp)
            Next i
        Else
            col2 = node.TopLeftCell.Column
            For i = 1 To nb_rows_above
                ws.Range(ws.Cells(new_node_row - 1, col2 - 1), ws.Cells(new_node_row - 1, col2)).Delete (xlShiftUp)
            Next i
        End If
        If nb_rows_above <> 4 Then
            'Need to readjust the starting point of all front arcs of the branches starting from the node
            For Each s In ws.Shapes
                p = InStr(s.Name, "FBranch " & current_node_nb)
                If p > 0 And Len(s.Name) = 9 + Len(current_node_nb) Then
                    Call New_FBranch(s.Left, s.Left + s.width, node.Top + node.height / 2, ws.Shapes(Find_Shape("Branch", Right(s.Name, Len(s.Name) - 8))).Top, Right(s.Name, Len(s.Name) - 8))
                End If
            Next s
        End If
    End If
    If current_node_nb <> "" Then
        ancestor_node_nb = Left(current_node_nb, Len(current_node_nb) - 1)
        rw = ws.Shapes(Find_Node(ancestor_node_nb)).TopLeftCell.Row
        col = ws.Shapes(Find_Node(ancestor_node_nb)).TopLeftCell.Column
        Call Delete_Rows_After_Leaf_Deletion(ancestor_node_nb, rw, col, False)
    End If

End Sub

Public Sub Readjust_Extended_Branches_After_Leaf_Deletion()
'Readjusts the position of all extended branches following a leaf deletion

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim branch As Shape
    
    For Each s In ws.Shapes
        p = InStr(s.Name, "XBranch")
        If IsNumeric(p) And p > 0 Then
            xbranch_nb = Right(s.Name, Len(s.Name) - 8)
            Set branch = ws.Shapes(Find_Shape("Branch", xbranch_nb))
            s.Top = branch.Top
        End If
    Next s

End Sub

Public Sub Prune_Node(node_shape_nb)
'Prunes / deletes a single node given its number, assuming that it only has one branch left

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim node_to_prune As Shape
    Dim c As Range
    
    Set node_to_prune = ws.Shapes(node_shape_nb)
    rw = node_to_prune.TopLeftCell.Row
    col = node_to_prune.TopLeftCell.Column
    lft = node_to_prune.Left
    y = node_to_prune.Top + node_to_prune.height / 2
    Thickness = ws.Rows(rw).RowHeight * 3 / 4
    branch_number = Replace(node_to_prune.Name, "TrNd ", "")
    node_name = node_to_prune.Name
    node_nb = Right(node_name, Len(node_name) - 5)
    Call Delete_Tree_Value(node_nb)
    'Erase the branch labels of the branch that just got deleted and which was a child of node_to_prune
    ws.Cells(rw - 1, col + 2).Value = ""
    ws.Cells(rw - 1, col + 3).Value = ""
    old_max_stage = Max_Stage2()
    node_to_prune.Delete
    If node_name = "TrNd " Then
        'If it is the root, i.e. the whole tree, that the user wishes to delete
        For Each s In ws.Shapes
            ws.Cells(rw, col + 5).Value = ""
            ws.Cells(rw, col + 6).Value = ""
            ws.Cells(rw - 4, col + 5).Value = ""
            ws.Cells(rw - 4, col + 6).Value = ""
            If s.Name = "Root " Then
                s.Delete
                Exit Sub
            End If
        Next s
    End If
    need_to_shift_cells_left = True
    For Each s In ws.Shapes
        If InStr(s.Name, "TrNd ") = 1 And Len(s.Name) >= Len(node_name) Then
            need_to_shift_cells_left = False
            Exit For
        End If
    Next s
    this_node_stage = Len(node_name) - 5
    
    'May need to readjust the size of the tree, more specifically the length of some branches
    If need_to_shift_cells_left Then
        For Each s In ws.Shapes
            
            On Error GoTo no_shape:
                If InStr(s.Name, "Leaf") = 1 Then
                    Set c = s.TopLeftCell
                    s.Left = s.Left + 5
                    ws.Range(ws.Cells(c.Row, c.Column - 5), ws.Cells(c.Row, c.Column - 1)).Delete (xlShiftToLeft)
                    s.Left = s.Left - 5
                End If
return_no_shape:
 
            On Error GoTo 0
        Next s
        this_node_stage = this_node_stage + 1
    End If
    lft2 = ws.Cells(1, col + (old_max_stage - this_node_stage + stage_adjust) * 5).Left
    With ws.Shapes.AddLine(lft2, y - Thickness / 2, lft2, y + Thickness / 2)
        .Name = "Leaf " & branch_number
    End With
    If lft <> lft2 Then
        With ws.Shapes.AddLine(lft, y, lft2, y)
            .Name = "XBranch " & branch_number
        End With
    End If
    If need_to_shift_cells_left Then
        col = col + (old_max_stage - this_node_stage + stage_adjust) * 5
        ws.Columns(col).ColumnWidth = 14
        ws.Columns(col + 1).ColumnWidth = 10
        ws.Cells(rw, col).Formula = ws.Cells(rw, col + 5).Formula
        Call U_Value_Leaf(rw, col + 1)
        ws.Cells(rw, col + 5).Formula = ""
        ws.Cells(rw, col + 6).Formula = ""
        'Look for the "Value Measure" and "U-Values" cells and shifts them to the left
        top_leaf_row = 65535
        For Each s In ws.Shapes
            If InStr(s.Name, "Leaf") = 1 Then
                this_leaf_row = s.TopLeftCell.Row
                If this_leaf_row < top_leaf_row Then top_leaf_row = this_leaf_row
            End If
        Next s
        top_leaf_row = top_leaf_row - 4
        For i = 0 To 1
            With ws.Cells(top_leaf_row, col + i)
                .Formula = ws.Cells(top_leaf_row, col + 5 + i).Formula
                .Font.Bold = ws.Cells(top_leaf_row, col + 5 + i).Font.Bold
                .Font.Underline = ws.Cells(top_leaf_row, col + 5 + i).Font.Underline
                .Font.Italic = ws.Cells(top_leaf_row, col + 5 + i).Font.Italic
            End With
            ws.Cells(top_leaf_row, col + 5 + i).Formula = ""
        Next i
    End If
    
    Call Tree_Values(Left(node_nb, Len(node_nb) - 1))

Exit Sub
no_shape:
    ' this shape has been deleted by excel 2007 so skip this block
Resume return_no_shape:
End Sub

Public Sub Prune_Nodes(node_shape_nb)
'Prunes / deletes a node and all its descendants given its number, assuming that it only has one branch left

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim node_to_prune As Shape
    Dim c As Range
    
    Set node_to_prune = ws.Shapes(node_shape_nb)
    node_nb = Right(node_to_prune.Name, Len(node_to_prune.Name) - 5)
    found_a_leaf_to_prune = False
    For i = 1 To ws.Shapes.Count
        If InStr(ws.Shapes(i).Name, "Leaf " & node_nb) = 1 Then
            found_a_leaf_to_prune = True
            Call Prune_Leaf(i)
            Exit For
        End If
    Next i
    If found_a_leaf_to_prune Then
        node_shape_nb = Find_Node(node_nb)
        If node_shape_nb <> -1 Then Call Prune_Nodes(node_shape_nb)
    Else
        Set leaf = ws.Shapes(Find_Shape("Leaf", node_nb_for_node_deletion))
        Set c = leaf.TopLeftCell
        c.Value = branch_CE
    End If
    
End Sub

