#!/bin/bash
TARGET="msande_teachingda/public/tools/"
FILE="SimpleDecisionTree.xlam"
echo "Deploying" $FILE "to" $TARGET "on Box.com"
read -p "Stanford email (must be member of msande_teachingda workgroup)\n: " email
read -s -p "Password: " password

curl -u $email:$password -T $FILE https://dav.box.com/dav/$TARGET
